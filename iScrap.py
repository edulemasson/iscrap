#!/usr/bin/env python
# -*- encoding:utf-8 -*-
import sys
import urllib2
import lxml
import simplejson
import os


from lxml import html
from subprocess import call

import requests
print '\n\n\n'

if len(sys.argv) == 1:
	print '\n\n ERROR: Video ID parameter missing'
	print '\n usage: python iScrap.py #YOUTUBE-ID#'
	print '\n\n Example: \n Youtube URL: https://www.youtube.com/watch?v=87D4SZ2tyC8'
	print '\n\n Command: python iScrap.py 87D4SZ2tyC8'

#TODO PEGAR VIDEO ID DO PARAM ARGV
id = sys.argv[1] #'33thdy0IjPE'

page = requests.get('https://youtu.be/'+ id)
tree = html.fromstring(page.content)

#Capturando o Titulo do Video
video_title  = tree.xpath('//*[@id="eow-title"]/text()')
title = video_title[0]

#Higieniza qualquer newline da identação do HTML
title ="".join(title.split())

print '\n Efetuando o download da Legenda do Video'


title = title.replace('|', "-")
#title = title.replace('&amp;#39;', "'")

print '\n Convertendo Titulo do Video'

response = urllib2.urlopen('http://video.google.com/timedtext?lang=pt-BR&v=' + id)
html = response.read()
response.close()  # best practice to close the file

if len(html) == 0:
	response = urllib2.urlopen('http://video.google.com/timedtext?lang=en&v=' + id)
	html = response.read()
	response.close()  # best practice to close the file

print "\n Tamanho da Legenda: " + str(len(html))

filename = title + "-" + id

print '\n Filename: ' + filename
htmlspaced = html.replace('\r\n', ' ')

f = open(filename + ".xml", "w")
f.write(htmlspaced)
f.close()

print '\n Convertendo Legenda para Formato Padrão SRT'
os.system("python include.py " + filename + ".xml")

print '\n Removendo TEMP Transcript file'
os.remove(filename + ".xml")

print '\n Realizando o Download do Video'
#os.system("youtube-dl --format mp4 --output "+ filename + ".mp4 https://www.youtube.com/watch?v="+id)

print '\n\n\n'
